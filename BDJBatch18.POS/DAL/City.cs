﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class City : MyBase
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public bool insert()
        {
            MyCommand = CommandBuilder(@"insert into city (name) values (@name)");
            MyCommand.Parameters.AddWithValue("@name", Name);
            return ExecuteNQ(MyCommand);
        }

        public bool update()
        {
            MyCommand = CommandBuilder(@"update city set name = @name where id = @id ");

            MyCommand.Parameters.AddWithValue("@id", Id);
            MyCommand.Parameters.AddWithValue("@name", Name);
            return ExecuteNQ(MyCommand);
        }

        public bool SelectById()
        {
            MyCommand = CommandBuilder(@"select id, name form city where id = @id");
            MyCommand.Parameters.AddWithValue("@id", Id);

            MyReader = ExecuteReader(MyCommand);

            while (MyReader.Read())
            {
                Name = MyReader["name"].ToString();
                return true;
            }
            return false;
        }

        public DataSet select()
        {
            MyCommand = CommandBuilder(@"Select id, name from city where id > 0 ");
            if (!string.IsNullOrEmpty(Search))
            {
                MyCommand.CommandText += "and name like @search";
                MyCommand.Parameters.AddWithValue("@search", "%" + Search + "%");
            }

            return ExecuteDataSet(MyCommand);
        }

    }
}
