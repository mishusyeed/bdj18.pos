﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class Transaction : MyBase
    {
        public int Id { get; set; }
        public int Number { get; set; }
        public string Reference { get; set; }
        public DateTime DateTime { get; set; }
        public int LedgerId { get; set; }
        public int EmployeeId { get; set; }
        public double Debit { get; set; }
        public double Credit { get; set; }
        public string Remarks { get; set; }

        public bool insert()
        {
            MyCommand = CommandBuilder(@"insert into transaction (number, reference, ledgerId, employeeId, debit, credit, remarks) values
                                    (@number, @reference, @ledgerId, @employeeId, @debit, @credit, @remarks)");
            MyCommand.Parameters.AddWithValue("@number", Number);
            MyCommand.Parameters.AddWithValue("@reference", Reference);
            MyCommand.Parameters.AddWithValue("@ledgerId", LedgerId);
            MyCommand.Parameters.AddWithValue("@employeeId", EmployeeId);
            MyCommand.Parameters.AddWithValue("@debit", Debit);
            MyCommand.Parameters.AddWithValue("@credit", Credit);
            MyCommand.Parameters.AddWithValue("@remarks", Remarks);
            return ExecuteNQ(MyCommand);
        }

        public bool update()
        {
            MyCommand = CommandBuilder(@"update transaction set number = @number, set reference = @reference, set ledgerId = @ledgerId, 
                        set employeeId = @employeeId, set debit =@debit, set credit = @cerdit, set remarks = @remarks where id = @id");
            MyCommand.Parameters.AddWithValue("@id", Id);
            MyCommand.Parameters.AddWithValue("@number", Number);
            MyCommand.Parameters.AddWithValue("@reference", Reference);
            MyCommand.Parameters.AddWithValue("@ledgerId", LedgerId);
            MyCommand.Parameters.AddWithValue("@employeeId", EmployeeId);
            MyCommand.Parameters.AddWithValue("@debit", Debit);
            MyCommand.Parameters.AddWithValue("@credit", Credit);
            MyCommand.Parameters.AddWithValue("@remarks", Remarks);
            return ExecuteNQ(MyCommand);
        }

        public bool delete ( string ids = "")
        {
            if(ids != null)
            {
                MyCommand = CommandBuilder(@"delete from transaction where id in " + ids + "");
            }
            return ExecuteNQ(MyCommand);
        }

        public bool selectById()
        {
            MyCommand = CommandBuilder(@"select id, number, reference, ledgerId, employeeId, debit, credit, remarks from transaction where id = @id");
            MyCommand.Parameters.AddWithValue("@id", Id);
            MyReader = ExecuteReader(MyCommand);
            while (MyReader.Read())
            {
                Number = Convert.ToInt32(MyReader["number"]);
                Reference = MyReader["reference"].ToString();
                LedgerId = Convert.ToInt32(MyReader["ledgerId"]);
                EmployeeId = Convert.ToInt32(MyReader["employeeId"]);
                Debit = Convert.ToDouble(MyReader["debit"]);
                Credit = Convert.ToDouble(MyReader["credit"]);
                Remarks = MyReader["remarks"].ToString();
                return true;
            }
            return false;
        }

        public DataSet select()
        {
            MyCommand = CommandBuilder(@"select ts.id, ts.number, ts.reference, ts.debit, ts.credit, ts.remarks, lg.name as Ledger, emp.name as Employee from transaction
                                        left join ledger as lg on ts.ledgerId = lg.id
                                        left join employee as emp on ts.employeeId = emp.id where ts.id > 0");
            if (!string.IsNullOrEmpty(Search))
            {
                MyCommand.CommandText += "and ts.number, ts.reference, ts.debit, ts.credit, ts.remarks like @search";
                MyCommand.Parameters.AddWithValue("@search", "%"+Search+"%");
            }
            if(LedgerId > 0)
            {
                MyCommand.CommandText += "and lg.id = @ledgerId";
                MyCommand.Parameters.AddWithValue("@ledgerId", LedgerId);
            }
            if(EmployeeId > 0)
            {
                MyCommand.CommandText += "and emp.id = @employeeId";
                MyCommand.Parameters.AddWithValue("@employeeId", EmployeeId);
            }
            return ExecuteDataSet(MyCommand);
        }
    }
}
