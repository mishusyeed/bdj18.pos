﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class ProductPrice : MyBase
    {
        public int ProductId { get; set; }
        public int UnitId { get; set; }
        public double Price { get; set; }

        public bool insert()
        {
            MyCommand = CommandBuilder(@"insert into productPrice (productId, unitId, price) values(@productId, @unitId, @price) ");
            MyCommand.Parameters.AddWithValue("@productId",ProductId);
            MyCommand.Parameters.AddWithValue("@unitId", UnitId);
            MyCommand.Parameters.AddWithValue("@price",Price);
            return ExecuteNQ(MyCommand);
        }

        public bool update()
        {
            MyCommand = CommandBuilder(@"update productPrice set productId = @productId, set unitId = @unitId, set price = @price where productId = @productId and unitId = @unitId");
            MyCommand.Parameters.AddWithValue("@productId", ProductId);
            MyCommand.Parameters.AddWithValue("@unitId", UnitId);
            MyCommand.Parameters.AddWithValue("@price", Price);
            return ExecuteNQ(MyCommand);
        }

        public bool delete()
        {
            MyCommand = CommandBuilder(@"delete from productPrice where productId = @productId and unitId = @unitId");
            return ExecuteNQ(MyCommand);
        }
        public bool selectById()
        {
            MyCommand = CommandBuilder(@"select productId, unitId, price from productPrice where productId = @productId and unitId = @unitId");
            MyCommand.Parameters.AddWithValue("@productId", ProductId);
            MyCommand.Parameters.AddWithValue("@unitId", UnitId);
            MyReader = ExecuteReader(MyCommand);

            while (MyReader.Read())
            {
                ProductId = Convert.ToInt32(MyReader["productId"]);
                UnitId = Convert.ToInt32(MyReader["unitId"]);
                Price = Convert.ToDouble(MyReader["price"]);
                return true;
            }
            return false;

        }

        public DataSet select()
        {
            MyCommand = CommandBuilder(@"select productId, unitId, price from productPrice");
            return ExecuteDataSet(MyCommand);
        }
    }
}
