﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class Brand : MyBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Origin { get; set; }

        public bool insert()
        {
            MyCommand = CommandBuilder(@"insert into brand (name, origin) values (@name, @origin)");
            MyCommand.Parameters.AddWithValue("@name", Name);
            MyCommand.Parameters.AddWithValue("@origin", Origin);

            return ExecuteNQ(MyCommand);
        }
        public bool update()
        {
            MyCommand = CommandBuilder(@"update brand set name = @Name , set origin = @origin where id = @id");
            MyCommand.Parameters.AddWithValue("@id", Id);
            MyCommand.Parameters.AddWithValue("@name", Name);
            MyCommand.Parameters.AddWithValue("@origin", Origin);

            return ExecuteNQ(MyCommand);
        }
        public bool delete(string ids = "")
        {
            if (ids != "")
            {
                MyCommand = CommandBuilder(@"delete from brand where id in " + ids + "");
            }
            return ExecuteNQ(MyCommand);
        }

        public bool selectById()
        {
            MyCommand = CommandBuilder(@"select id , name, origin from brand where id = @id");
            MyCommand.Parameters.AddWithValue("@id", Id);
            MyReader = ExecuteReader(MyCommand);

            while (MyReader.Read())
            {
                Name = MyReader["name"].ToString();
                Origin = MyReader["origin"].ToString();
                return true;
            }
            return false;
        }

        public DataSet select()
        {
            MyCommand = CommandBuilder(@"select id, name, origin form brand where id > 0");
            if (!string.IsNullOrEmpty(Search))
            {
                MyCommand.CommandText += "and name, origin like @search";
                MyCommand.Parameters.AddWithValue("@search", Search);
            }
            return ExecuteDataSet(MyCommand);
        }

    }
}
