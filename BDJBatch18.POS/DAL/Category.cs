﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    class Category : MyBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CategoryId { get; set; }

        public bool insert ()
        {
            MyCommand = CommandBuilder(@"insert into category (name, description, categoryId) values (@name, @description, @categoryId)");
            MyCommand.Parameters.AddWithValue("@name", Name);
            MyCommand.Parameters.AddWithValue("@description", Description);
            MyCommand.Parameters.AddWithValue("@categoryId", CategoryId);

            return ExecuteNQ(MyCommand);
        }

        public bool update()
        {
            MyCommand = CommandBuilder(@"update category set name = @name, set description = @description, set categoryId = @categroyId where id = @id");
            MyCommand.Parameters.AddWithValue("@id", Id);
            MyCommand.Parameters.AddWithValue("@name", Name);
            MyCommand.Parameters.AddWithValue("@description", Description);
            MyCommand.Parameters.AddWithValue("@categoryId", CategoryId);

            return ExecuteNQ(MyCommand);
        }
        public bool delete (string ids = "")
        {
            if(ids != "")
            {
                MyCommand = CommandBuilder(@"delete from category where id in " + ids + "");
            }
            return ExecuteNQ(MyCommand);
        }

        public bool selectById()
        {
            MyCommand = CommandBuilder(@"select id, name, description, categoryId from category where id = @id");
            MyCommand.Parameters.AddWithValue("@id", Id);
            MyReader = ExecuteReader(MyCommand);

            while(MyReader.Read())
            {
                Name = MyReader["name"].ToString();
                Description = MyReader["description"].ToString();
                CategoryId = Convert.ToInt32(MyReader["categoryId"]);
                return true;
            }
            return false;
        }
        public DataSet select()
        {
            MyCommand = CommandBuilder(@"select id, name, description, categoryId form category where id > 0");
            if (!string.IsNullOrEmpty(Search))
            {
                MyCommand.CommandText += "and name, description, categoryId like @search";
                MyCommand.Parameters.AddWithValue("@search", Search);
            }
            return ExecuteDataSet(MyCommand);
        }
    }
}
